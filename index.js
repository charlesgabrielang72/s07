/*Learning Objectives:
	- create code that will execute only when a given condition has been made
Topics:
	- Assignment Operators
	- Conditional Statements
	- Ternary Operator */

// Lesson Proper
/*Conditional Statements
	- a conditional statement is one of the key features of a programming language

	Ex.
		is the container full or not?
		is the temparature higher than or equal to 40 degrees celcius?
		does the title contain an ampersand(&) character? 
There are three types of conditional statements: 
 - if-else statements
 - switch statements
 - try-catch-finally statements 

 Operators
 	- allow programming languages to execute operations or evaluations

	Assignment Operators
	- assign a value to a variable
 */
// [SECION] Basic Assignment Operator (=)
	// it allows us to assign a value to a variable
	let variable = "initial value";

// Mathematical Operators (addition (+), subtraction (-), multiplication (*), division(/), modulo(%))
// Whenever you have used a mathematical operator, a value is returned,it is only up to us if we save taht returned value.
// Addition, subtraction, multi[lication, division assignment operators allows us to assign the result of the operation to the value of the left operand. It allows us to save the result  of a mathematical operation to the left operand

	let num1 = 5;
	let num2 = 10;    
	let num3 = 4;
	let num4 = 40;

	/*
	    Addition Assignment Operator (+=): 
	     left operand 
	      - is the variable or value of the left side of the operator
	     right operand
	      - is the variable or value of the right side of the operator
	      ex. sum = num1 + num4 > re-assigned the value of sum with the result of  num1 + num4 
	 */
	// num1 = num1 + num4; 
	num1 += num4;
	console.log(num1);

	num2 += num3;
	console.log(num2);

	num1 += 55;
	console.log(num1);
	console.log(num4); //40 - previous right should not be affected/re-assigned

	let string1 = "Boston";
	let string2 = "Celtics";
	// string1 = string1 + string2
	string1 += string2;
	console.log(string1);//BostonCeltics - results in concatenation between 2 strings and saves the results in the left operand
	console.log(string2);//Celtics

	// 15 += num1; it produces an error, because we do not use assignment operator when the left operand is just a value/data
	
	// subtraction assignment operator (-=)
	num1 -= num2;
	console.log("Result of subtraction assignment operator: " + num1);
	// multiplication assignment operator (*=)
	num2 *= num3;
	console.log("Result of multiplication assignment operator: " + num2);

	//division multiplication assignment operator (/=)
	num4 /= num3;
	console.log("Result of division assignment operator: " + num4);

// [SECTION] Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = y - x;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulus operator: " + remainder);

// Multiple Operators and Parenthesis
/*
	- when multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	- the operations were done in the following order:
		expression = 1 + 2 - 3 * 4 / 5;
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
 */

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of multiple operations: " + mdas);

// order of operation can be changed by adding parentheses to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
/*
	- by adding parentheses "()", the order of operation are changed prioritizing operations inside the parentheses first then follow mdas rule
	- the operation were done in the following order:
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.19
 */
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation 2nd example: " + pemdas)

// Increment and Decrement
	// Increment and decrement is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment or decrement was used
	//2 Kinds of Incrementation: Pre-fix and Post-fix
	
	let z = 1;

	// Pre-fix Incrementation
	++z;
	console.log("Pre-fix increment: " + z);

	// Post-fix Incrementation
	z++;
	console.log("Post-fix increment: " + z);//3 - The value of z was added with 1
	console.log(z++);//3 - with post-incrementation the previous value of the variable is returned first before the actual incrementation 
	console.log(z);//4 - new value is now returned

	// Pre-fix vs Post-fix
	console.log(z);
	console.log(z++);//4 previous value was returned first
	console.log(z);//5 new value is now returned

	console.log(++z);

	// Pre-fix and Post-fix Decrementation
	console.log(--z);
	console.log(z--);
	console.log(z);

// Comparison Operators
	/* Comparison Operators 
		- are used to compare the values of the left and right operands
		- comparison operators return boolean
			- Equality or Loose equality operator (==)
			- Strict equality operator (===)
	*/
	console.log(1 == 1);//true

	let isSame = 55 == 55;
	console.log(isSame);

	console.log(1 == '1');//true - loose equality operator priority is the sameness of the value because with loose equality operator, forced coercion is done before comparison - JS forcibly changes the data type to the operands

	console.log(0 == false);//true - with forced coercion, false was converted into a number results into Nan so therefore, 1 is not equal to NaN
	console.log(1 == true);//true
	console.log("false" == false);//false
	console.log(true == "true");//false
	/*
	  	with loose comparison operator (==), values are compared and types if operands do not have the same types, it will be forced coerced/type coerce before comparison value 

	  	if either the operand is a number of boolean, the operands are converted into numbers.
	*/
	console.log(true == "1");//true - true coerced into a number = 1, "1" converted into a number = 1 - 1 equals 1

	// Strict Equality (===)
	console.log(true === "1");//false

	// check both value and type
	console.log(1 === '1'); //false - operands have the same valued but different types
	console.log("BTS" === "BTS");//true - same value and same typed
	console.log("Marie" === "marie");//false - left operand is capitalized and right operand is small caps

	// Inequality Operators (!=)
		/* Loose inequality operators
			- checks whether the operands are NOT equal or have different values
			- will do type coercion if the operancs have different
		*/
	console.log('1' != 1);//false
	/*
		false = both operands were converted to numbers
			'1' converted into number is 1
			1 converted into number is 1 - 1 equals 1 not inequal
	 */	
	console.log("James" != "John");
	console.log(1 != true);//false
	/*
	   false - with type conversion: true was converted to 1
	   1 is equal to 1
	   it is NOT inequal
	 */
	
	// Strict inequality (!==) - checks whether the two operands have different values and will check if they have different types
	console.log("5" !== 5);//true - operands are inequal because they have different types
	console.log(5 !== 5);//false - operands are equal they have the same value and they have the same type

	let name1 = "Jin";
	let name2 = "Jimin";
	let name3 = "Jungkook";
	let name4 = "V";

	let number1 = 50;
	let number2	= 60;
	let numString1 = "50";
	let numString2 = "60";

	console.log(numString1 == number1);//true
	console.log(numString1 === number1);
	console.log(numString1 != number1);
	console.log(name4 !== "num3");
	console.log(name3 == "Jungkook");
	console.log(name1 === "Jin");

/* Relational comparison operators
	a comparison operator compares its operand and returns a boolean value based on whether the comparison is true
*/ 

	let a = 500;
	let b = 700;
	let c = 8000;
	let numString3 = "5500";

	// Greater than (>)
	console.log(a > b);
	console.log(c > y);

	// Less than (<)
	console.log(c < a);//false
	console.log(b < b);//false
	console.log(a < 1000);//true
	console.log(numString3 < 1000);//false - forced coercion to change the string into number
	console.log(numString3 < 6000);//true
	console.log(numString3 < "Jose");//true - "5500" < "Jose" - that is erratic (unpredictable)

	// Greater than or equal to (>=)
	console.log(c >= 10000);//false
	console.log(b >= a);//true

	// Less than or equal to (<=)
	console.log(a <= b);//true
	console.log(c <= a);//false
/* Logical Operators
	AND operator (&&)
		- both operands on the left and right or all operands must be true otherwise it is false

		T && T = true
		T && F = false
		F && T = false
		F && F = false
*/

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1);//false

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2);//true

	let requiredLevel = 95;
	let requiredAge = 18;

	let authorization3 = isRegistered && requiredLevel === 25;
	console.log(authorization3);//false

	let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization4);//true

	let userName = "gamer2001";
	let userName2 = "shadow1991";
	let userAge = 15;
	let userAge2 = 30;

	let registration1 = userName.length > 8 && userAge >= requiredAge;
	// .length is a property of string which determine the number of characters in the string.
	console.log(registration1);//false - userAge does not meet the required age = 18

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2);//true

	/* OR Operator (|| - double pipe)
		- returns true if at least one of the operands are true.
		T || T  = true
		T || F = true
		F || T = true
		F || F = false */ 

	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirement1);//false

	let guildRequirement2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement2);//true

	let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement3);//true

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin);//false

	// Not Operator - it turns a boolean into the opposite value
	
	let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin2);//true

	console.log(!isRegistered);

//[SECTION] Conditional
	//if-else statements - will run a block of code if the condition specified is true or results to true
	
	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	// if(true){
	// 	alert("We just run an if condition!")
	// };
	
	if(userName3.length > 10){
		console.log("Welcome to Game Online!");
	};

	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the guild.");
	};

	// else statement will be run if the condition given is false or results to false
	if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
			console.log("Thank you for joining the Noobies Guild");
	 } else {
	 	console.log("You are too strong to be a noob. :(");
	};

	// else if - executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true
	if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge){
			console.log("Thank you for joining the noobies guild.");
	} else if(userLevel3 > 25){
			console.log("You are too strong to be a noob.");
	} else if(userAge3 < requiredAge){
			console.log("You are too young to join the guild.");
	} else if(userName3.length < 10){
		console.log("Username too short.")
	};

	// if-else in function
	function addNum(num1, num2){
		// check if the numbers being passed argumetn are number types
		// typeof keyword returns a string which tells the type of data that follow it
		if(typeof num1 === 'number' && typeof num2 === 'number'){
			console.log("run only if both arguments passed are number types.");
			console.log(num1 + num2);
		} else {
			console.log("One or both of the arguments are not numbers");
		};
	};

	addNum(5,"2");
	function login(userName1,userName2){
           if(typeof userName1 ==='string'&& typeof userName2 === 'string'){
            console.log("Both arguments are strings");
        }
        if(userName1.length>=8 && userName2.length>=8)
           {
                console.log("logging in")
           }
            else if (userName2.length <8)
           {
                alert("password is too short")
           }
            else if (userName1.length<8)
           {
                alert("username is too short")
           }
            else{
                alert("Credentials are too short");
           }
    }
    login('Jhinbilog','darkhorse');

  function shirtColor(Day){
	let day = Day.toLowerCase();
	let colorShirt = "";

	if(typeof day === 'string'){
			console.log("The argument is a string")
		}

	if(day == "monday"){
		colorShirt = "Black"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "tuesday"){
		colorShirt = "Green"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "wednesday"){
		colorShirt = "Yellow"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "thursday"){
		colorShirt = "Red"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "friday"){
		colorShirt = "Violet"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "saturday"){
		colorShirt = "Blue"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else if(day == "sunday"){
		colorShirt = "White"
		alert("Today is " + day + " Wear " + colorShirt)
	}

	else{alert("Please input a proper input");
	}
}

shirtColor("TueSday");
/*Switch Statement
	- is used an alternative to an if, else-if or else tree, where the data being evaluated or checked in an expected input
	- if we want to select one of many code blocks/statements to be executed
	syntax:
		switch(expression/condition){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/
let hero = "Anpanman";
switch(hero){
	case "Jose Rizal":
       console.log("Philippines National Hero");
       break;
    case "George Washington":
        console.log("Hero of the American Revolution");
        break
    case "Hercules":
         console.log("Legendary Hero of the Greek");
         break;
    case "Anpanman":
         console.log("Superhero!");
         break;
};

function roleChecker(role){
	switch(role){
		case "Admin":
		    console.log("Welcome Admin, to the Dashboard.");
		    break;
		case "User":
		    console.log("You are not authorized to view this page.");
		    break;
		case "Guest":
		     console.log("Go to the registration page to register.");
		    break;
		    // break it breaks/terminates the code block. If this was not added to your case then, the next case will run as well.
		default:
		// by default your switch ends with default case, so therefore,even if there is no break keyword in your default case, it will not run anything else
		    console.log("Invalid Role.") 
	};
};

roleChecker("Admin");

/*
	Mini-Activity
	  create a colorOfTheDay function, instead of using if-else, convert it to a switch(use switch statement)
 */

// function with if-else and return
function gradeEvaluator(grade){
	/*
		evaluate the grade input and return the letter distinction
			 - if the grade is less than or equal to 70 = F
			 - if the grade is greater than or equal to 71 = C
			 - if the grade is greater than or equal to 80 = B
			 - if the grade is greater than or equal to 90 = A
		resume: 3:45 PM	
	*/
	if(grade >= 90){
		/*return keyword can be used in an if-else statement inside a function
			- it allows the function to return a value
		*/
		return "A";
	} else if(grade >= 80){
		return "B";
	} else if(grade >= 71){
		return "C";
	} else if(grade <= 70){
		return "F"
	} else {
		return "Invalid"
	};
};

let grade = gradeEvaluator(75);
console.log(`The result is: ${grade}`);

/* Ternary Operators*/
	// shorthand way of writing if-else statements
	/*
		syntax:
		condition ? if-statement : else statement
	 */
   
   
/*Activity*/
// Instruction:


// 	Create two functions:

// 		-First function: oddEvenChecker
// 			-This function will check if the number input or passed as an argument is an odd or even number.
// 				-Check if the argument being passed is a number or not.
// 					-if it is a number, check if the number is odd or even.
// 					-even numbers are divisible by 2.
// 					-log a message in the console: "The number is even." if the number passed as argument is even.
// 					-log a message in the console: "The number is odd." if the number passed as argument is odd.
// 					-if the number passed is not a number type: 
// 						show an alert:
// 						"Invalid Input."

// 		-Second Function: budgetChecker()
// 			-This function will check if the number input or passed as an argument is over or is less than the recommended budget.
// 				-Check if the argument being is a number or not.
// 					-if it is a number check if the number given is greater than 40000
// 						-log a message in the console: ("You are over the budget.")
// 					-if the number is not over 40000:
// 						-log a message in the console: ("You have resources left.")
// 					-if the argument passed is not a number:
// 						-show an alert: ("Invalid Input.")


// Pushing Instructions:

// Go to Gitlab:
// 	-in your projects folder and access your folder.
// 	-create s07  project folder 
// 	-untick the readme option
// 	-copy the git url from the clone button of your activity repo.

// Go to Gitbash:
// 	-go to your s07 folder and access activity folder
// 	-initialize activity folder as a local repo: git init
// 	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
// 	-add your updates to be committed: git add .
// 	-commit your changes to be pushed: git commit -m "includes repetition control activity"
// 	-push your updates to your online repo: git push origin master
